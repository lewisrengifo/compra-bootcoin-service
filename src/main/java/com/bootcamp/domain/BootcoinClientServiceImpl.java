package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.BootcoinClient;
import com.bootcamp.infraestructure.repository.BootcoinClientRepository;
import com.bootcamp.infraestructure.service.BootcoinClientService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** Implementation for bootcoinClient. */
@Slf4j
@Service
public class BootcoinClientServiceImpl implements BootcoinClientService {

  @Autowired private BootcoinClientRepository bootcoinClientRepository;

  @Override
  public Flux<BootcoinClient> getAll() {
    return bootcoinClientRepository.findAll();
  }

  @Override
  public Mono<BootcoinClient> saveBootcoinClient(BootcoinClient bootcoinClient) {
    return bootcoinClientRepository.save(bootcoinClient);
  }

  @Override
  public Mono deleteById(String id, BootcoinClient bootcoinClient) {
    return bootcoinClientRepository.deleteById(id);
  }

  @Override
  public Mono findById(String id, BootcoinClient bootcoinClient) {
    return bootcoinClientRepository.findById(id);
  }
}
