package com.bootcamp.domain;

import com.bootcamp.events.Event;
import com.bootcamp.events.EventType;
import com.bootcamp.events.PurchaseRequestCreatedEvent;
import com.bootcamp.infraestructure.document.PurchaseRequest;
import java.util.Date;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/** Purchase Request events service. */
@Component
public class PurchaseRequestEventsService {

  @Autowired private KafkaTemplate<String, Event<?>> producer;

  @Value("${topic.purchaseRequestCompra.name:purchaseRequestCompra}")
  private String topicBankAccount;

  public void publish(PurchaseRequest purchaseRequest) {
    PurchaseRequestCreatedEvent createdEvent = new PurchaseRequestCreatedEvent();
    createdEvent.setData(purchaseRequest);
    createdEvent.setId(UUID.randomUUID().toString());
    createdEvent.setType(EventType.CREATED);
    createdEvent.setDate(new Date());

    this.producer.send(topicBankAccount, createdEvent);
  }
}
