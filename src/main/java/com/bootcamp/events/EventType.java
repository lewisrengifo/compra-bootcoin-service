package com.bootcamp.events;

/** enum eventType. */
public enum EventType {
  CREATED,
  UPDATED,
  DELETED
}
