package com.bootcamp.events;

import com.bootcamp.infraestructure.document.PurchaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** Created event for purchase request. */
@Data
@EqualsAndHashCode
public class PurchaseRequestCreatedEvent extends Event<PurchaseRequest> {}
