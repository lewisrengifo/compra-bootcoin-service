package com.bootcamp.events;

import java.util.Date;
import lombok.Data;

/** Abstract class for event. */
@Data
public abstract class Event<T> {
  private String id;
  private Date date;
  private EventType type;
  private T data;
}
