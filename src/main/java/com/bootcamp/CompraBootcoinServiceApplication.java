package com.bootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** Spring application. */
@SpringBootApplication
public class CompraBootcoinServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(CompraBootcoinServiceApplication.class, args);
  }
}
