package com.bootcamp.infraestructure.document;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/** BootCoinClient document. */
@Getter
@Setter
@AllArgsConstructor
@Document(collection = "bootcoinClient")
public class BootcoinClient {
  @Id private String id;
  private String clientName;
  private String document;
  private String phoneNumber;
  private String imei;
  private String email;
}
