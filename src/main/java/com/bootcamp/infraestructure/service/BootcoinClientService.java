package com.bootcamp.infraestructure.service;

import com.bootcamp.infraestructure.document.BootcoinClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
/** BootCoinClient service. */
public interface BootcoinClientService {

  Flux<BootcoinClient> getAll();

  Mono<BootcoinClient> saveBootcoinClient(BootcoinClient bootcoinClient);

  Mono deleteById(String id, BootcoinClient bootcoinClient);

  Mono findById(String id, BootcoinClient bootcoinClient);
}
