package com.bootcamp.infraestructure.repository;

import com.bootcamp.infraestructure.document.BootcoinClient;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
/** BootCoinClient repository. */
@Repository
public interface BootcoinClientRepository extends ReactiveMongoRepository<BootcoinClient, String> {}
