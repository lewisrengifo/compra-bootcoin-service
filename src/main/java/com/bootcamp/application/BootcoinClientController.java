package com.bootcamp.application;

import com.bootcamp.infraestructure.document.BootcoinClient;
import com.bootcamp.infraestructure.service.BootcoinClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
/** BootCoinClient controller. */
@RestController
@RequestMapping("/bootcoinClient")
public class BootcoinClientController {

  @Autowired private BootcoinClientService bootcoinClientService;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<BootcoinClient> saveBootcoinClient(@RequestBody BootcoinClient bootcoinClient) {
    return bootcoinClientService.saveBootcoinClient(bootcoinClient);
  }
}
