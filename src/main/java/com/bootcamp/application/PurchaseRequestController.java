package com.bootcamp.application;

import com.bootcamp.domain.PurchaseRequestEventsService;
import com.bootcamp.infraestructure.document.PurchaseRequest;
import com.bootcamp.infraestructure.service.PurchaseRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/** Purchase Request controller. */
@RestController
@RequestMapping("/purchaseRequest")
public class PurchaseRequestController {

  @Autowired private PurchaseRequestService purchaseRequestService;

  @Autowired private PurchaseRequestEventsService purchaseRequestEventsService;

  @PostMapping("/savings")
  @ResponseStatus(HttpStatus.CREATED)
  public Mono<PurchaseRequest> savePurchaseRequest(@RequestBody PurchaseRequest purchaseRequest) {
    purchaseRequestEventsService.publish(purchaseRequest);
    return purchaseRequestService.save(purchaseRequest);
  }
}
